package com.classpath.messageheaders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MessageHeadersApplication {

    public static void main(String[] args) {
        SpringApplication.run(MessageHeadersApplication.class, args);
    }

}
