package com.classpath.messageheaders.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Configuration
@Slf4j
public class ConsumerClient {

    @Bean
    public Function<Message<String>, Message<String>> processMessage(){
        return (message) -> {
            //message.getHeaders().values().stream().forEach(header -> log.info(" Header "+ header));
            message.getHeaders()
                    .entrySet()
                    .stream()
                    .filter(stringObjectEntry -> stringObjectEntry.getKey().startsWith("order_"))
                    .forEach(stringObjectEntry -> {
                        System.out.println(stringObjectEntry.getKey()+ ": "+stringObjectEntry.getValue()+" \t");
                    });
            return message;
            };
    }

    @Bean
    public Supplier<Message<String>> generate(){
        return () -> MessageBuilder
                            .withPayload(UUID.randomUUID().toString())
                            .setHeader("order_app", "order-microservice")
                            .setHeader("order_correlation-id", UUID.randomUUID().toString())
                            .setHeader("order_reply-to", "ORDER_RES_Q")
                            .build();
    }
}